# Final Project Batch 41 Laravel

Kelompok 8
-

Anggota
-
- Ivan Febriand Muhammad
- Alfikiyar Tirta Haidar

Tema
-
Kami mengangkat tema forum diskusi dimana user dapat membuat pertanyaan, kategori, dan menjawab pertanyaan dari user lain.

Template
-
Forum19 - Bootstrap Template
https://elements.envato.com/forum-19-html-template-B47HMGV

Library/Package
-
- TinyMCE - https://www.tiny.cloud
- Laravel File Manager by Unisharp - https://unisharp.github.io/laravel-filemanager
- Carbon by Nesbot - https://carbon.nesbot.com
- Avatar by laravolt - https://github.com/laravolt/avatar

ERD
-
<p align="center"><img src="https://i.ibb.co/gSDJ3L0/Whats-App-Image-2023-01-22-at-17-02-16.jpg" alt="Whats-App-Image-2023-01-22-at-17-02-16" border="0"></p>

Link
-
- Link Demo Aplikasi - https://youtu.be/i06W9vXJlYY
- Link Deploy - https://forumgd.sanbercodeapp.com
