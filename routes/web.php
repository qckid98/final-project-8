<?php

use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RepliesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
     \UniSharp\LaravelFilemanager\Lfm::routes();
 });



Route::get('/', function () {
    return view('index');
});
Route::get('/', [TopicController::class, 'index'])->name('topics.index');
Route::get('/topic/create', [TopicController::class, 'create'])->middleware('auth');
Route::post('/topic/kirim', [TopicController::class, 'store'])->middleware('auth')->name('topics.store');
Route::get('/topic/{id}', [TopicController::class, 'show'])->name('topics.show');
Route::get('/topic/{id}/edit', [TopicController::class, 'edit'])->middleware('auth')->name('topics.edit');
Route::put('/topic/{id}/edit', [TopicController::class, 'update'])->middleware('auth')->name('topics.update');
Route::delete('/topic/{id}/delete', [TopicController::class, 'destroy'])->middleware('auth')->name('topics.destroy');
Route::get('/search', [TopicController::class, 'search'])->name('topics.search');
// Route::get('/topic/1' , [TopicController::class, 'show']);

// CRUD Kategori 
Route::resource('/kategori',KategoriController::class)->middleware('auth');

// Authentication Laravel UI
Auth::routes();

// Testing Only
Route::get('test', function () {
    return view('test');
});

Route::resource('/profile', ProfileController::class)->middleware('auth');
Route::post('/topic/{topic}/reply', [RepliesController::class, 'store'])->middleware('auth')->name('replies.store');
Route::get('/topic/{topic}/reply/{reply}/edit', [RepliesController::class, 'edit'])->middleware('auth')->name('replies.edit');
Route::put('/topic/{topic}/reply/{reply}/edit', [RepliesController::class, 'update'])->middleware('auth')->name('replies.update');
Route::delete('/topic/{topic}/reply/{reply}/delete', [RepliesController::class, 'destroy'])->middleware('auth')->name('replies.destroy');
