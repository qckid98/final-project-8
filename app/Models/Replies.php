<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Replies extends Model
{
    use HasFactory;

    protected $table = 'jawabanuser';
    protected $fillable = ['konten', 'user_id', 'pertanyaan_id'];

    public function user() {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }

    public function topic() {
        return $this->belongsTo(Topic::class , 'pertanyaan_id', 'id');
    }
}
