<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;

    protected $table = 'pertanyaan';
    protected $fillable = [
        'judul',
        'pertanyaan',
        'kategori_id',
        'user_id',
    ];

    public function user() {
        return $this->belongsTo(User::class , 'user_id', 'id');
    }

    public function kategori() {
        return $this->belongsTo(Kategori::class , 'kategori_id', 'id');
    }

    public function replies() {
        return $this->hasMany(Replies::class , 'pertanyaan_id', 'id');
    }
}
