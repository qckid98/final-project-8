<?php

    function address() {
        $id = Auth()->user()->id;
        $address = DB::table('profile')->where('user_id', $id)->first();
        $adressAlamat = $address->alamat;
        return $adressAlamat;
    }

    function biodata() {
        $id = Auth()->user()->id;
        $profile = DB::table('profile')->where('user_id', $id)->first();
        $biodata = $profile->biodata;
        return $biodata;
    }

    function nama() {
        $id = Auth()->user()->id;
        $user = DB::table('users')->where('id', $id)->first();
        $nama = $user->name;
        return $nama;
    }

    function email() {
        $id = Auth()->user()->id;
        $user = DB::table('users')->where('id', $id)->first();
        $email = $user->email;
        return $email;
    }

    function umur() {
        $id = Auth()->user()->id;
        $profile = DB::table('profile')->where('user_id', $id)->first();
        $umur = $profile->umur;
        return $umur;
    }





?>