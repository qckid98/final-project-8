<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;
use App\Models\Topic;
use App\Models\User;
use App\Models\Replies;

class ProfileController extends Controller
{
    public function show($id)
    {
        $reply = Replies::where('user_id', $id)->with(['user', 'topic'])->latest()->get();
        $profile = Profile::where('user_id', $id)->with('user')->first();
        $topic = Topic::where('user_id', $id)->with(['kategori', 'user'])->latest()->get();
        $userCount = User::withCount('topics', 'replies')->where('id', $id)->first();
        return view('profile.index', \compact('profile', 'topic' , 'userCount', 'reply'));

    }

    public function update(Request $request)
    {
        $id = Auth()->user()->id;
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'alamat' => 'required',
            'biodata' => 'required',
            'umur' => 'required',
        ]);
        $user = User::where('id', $id)->first();
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        $profile = Profile::where('user_id', $id)->first();
        $profile->update([
            'alamat' => $request->alamat,
            'biodata' => $request->biodata,
            'umur' => $request->umur,
        ]);
        return redirect()->route('profile.show', $id);
    }

    }
