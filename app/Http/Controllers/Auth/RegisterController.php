<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Profile;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Avatar;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'umur' => ['required'],
            'gender' => ['required'],
            'bio' => ['required'],
            'alamat' => ['required'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'avatar' => 'null',
            'password' => Hash::make($data['password']),
        ]);
        

        Profile::create([
            'umur' => $data['umur'],
            'alamat' => $data['alamat'],
            'gender' => $data['gender'],
            'biodata' => $data['bio'],
            'user_id' => $user->id,
        ]);

        $path = \storage_path('app/public/avatars/').$user->id.'.png';
        $avatar = Avatar::create($data['name'])->save($path);
        
        $user->avatar = $user->id.'.png';
        $user->update();

        return $user;
        dd($data);
    }
}
