<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Topic;
use App\Models\Replies;
use Auth;

class TopicController extends Controller
{
    public function create()
    {  
        $category = Kategori::all();
        return view('topics.create', \compact('category'));
    }

    public function store(Request $request) 
    {   
        $request->validate([
            'judul' => 'required',
            'tinyMCE' => 'required',
            'kategori_id' => 'required',
        ]);

        $topicC = Topic::create([
            'judul' => $request->judul,
            'pertanyaan' => $request->tinyMCE,
            'kategori_id' => $request->kategori_id,
            'user_id' => Auth()->user()->id,
        ]);

        $topicC->save();

        $replies = Replies::where('pertanyaan_id', $topicC->id)->with(['user'])->get();
        $topic = Topic::where('id', $topicC->id)->with(['kategori', 'user'])->first();
        $topics = Topic::with('kategori', 'user')->inRandomOrder()->limit(5)->get();

        return redirect('/topic/'. $topicC->id)->with(compact('topicC', 'topics', 'topic', 'replies'));
    }

    public function show(Request $request,$id)
    {   
        $replies = Replies::where('pertanyaan_id', $id)->with(['user'])->get();
        $topic = Topic::where('id', $id)->with(['kategori', 'user'])->first();
        $topics = Topic::with('kategori', 'user')->inRandomOrder()->limit(5)->get();
        return view('topics.single' , compact('topic', 'topics', 'replies'));
    }

    public function index(){
        
        $topics = Topic::with('kategori', 'user')->latest('created_at')->get();
        return view('index', \compact('topics'));
    }

    public function edit($id){
        $userID = Auth::user()->id;
        $topic = Topic::where('id', $id)->with(['kategori', 'user'])->first();
       $category = Kategori::all();
       return view('topics.edit', \compact('topic', 'category', 'userID'));
    }

    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required',
            'tinyMCE' => 'required',
            'kategori_id' => 'required',
        ]);

        $topic = Topic::find($id);
        $topic->judul = $request->judul;
        $topic->pertanyaan = $request->tinyMCE;
        $topic->kategori_id = $request->kategori_id;
        $topic->update();

        return redirect('/topic/'.$id);
    }

    public function destroy($id){
        $topic = Topic::find($id);
        $topic->delete();

        return redirect('/');
    }

    public function search(Request $request){
        $search = $request->get('search');
         $topics = Topic::with('kategori', 'user')->where('judul', 'like', '%'.$search.'%')->get();
        return view('/search', \compact('topics'));
    }

}
