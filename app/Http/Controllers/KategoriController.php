<?php

namespace App\Http\Controllers;
use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    // fetch all data from tabel kategori 
    public function index(){
        $kategori = Kategori::all();
        return view('pages.kategori.index', compact('kategori'));
    }

    // Create & Store data kategori 
    public function create(){
        return view('pages.kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        Kategori::create([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi,
        ]);

        return redirect('/kategori');
    }

    // Edit dan Update data kategori 
    public function edit($id){
        $kategori = Kategori::find($id);
        return view('pages.kategori.edit', compact('kategori'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        $kategori = Kategori::find($id);
        $kategori->nama = $request->nama;
        $kategori->deskripsi = $request->deskripsi;
        $kategori->save();

        return redirect('/kategori');
    }

    public function show($id){
        $kategori = Kategori::find($id);
        return view('pages.kategori.show', compact('kategori'));
    }

    public function destroy($id){
        $kategori = Kategori::find($id);
        $kategori->delete();

        return redirect('/kategori');
    }
}
