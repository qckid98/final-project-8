<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Replies;
use Auth;

class RepliesController extends Controller
{
    public function store(Request $request) 
    {   
        $request->validate([
            'message' => 'required',
        ]);

        $reply = Replies::create([
            'konten' => $request->message,
            'user_id' => Auth()->user()->id,
            'pertanyaan_id' => $request->topic_id,
        ]);

        $reply->save();

        return redirect()->back();
    }

    public function edit(Request $request){
        $id = $request->replyId;
        $reply = Replies::where('id', $id)->with(['user', 'topic'])->first();
        return view('replies.edit', \compact('reply', 'id'));
    }

    public function update(Request $request){
        $request->validate([
            'message' => 'required',
        ]);
        $id = $request->replyId;
        $reply = Replies::where('id', $id)->with(['user'])->first();
        $reply->konten = $request->message;
        $reply->update();

        return redirect('/topic/'.$reply->pertanyaan_id);
    }

    public function destroy(Request $request){
        $id = $request->replyId;
        $reply = Replies::where('id', $id)->with(['user'])->first();
        $reply->delete();

        return redirect()->back();
    }
}
