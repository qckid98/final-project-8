<div>
    <!-- Let all your things have their places; let each part of your business have its time. - Benjamin Franklin -->
    <script src="https://cdn.tiny.cloud/1/x9bh8x11ihsv65kouujvtlxkhasywr91avyh60k7ubm1bfh4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#tinyMCE', // Replace this CSS selector to match the placeholder element for TinyMCE
    plugins: 'code table lists image emoticons media wordcount',
    toolbar: 'undo redo | blocks | bold italic emoticons | alignleft aligncenter alignright | indent outdent | bullist numlist | code image media | table'
  });
</script>
</div>