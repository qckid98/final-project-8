@extends('layouts.master')
@section('title')
	Search - Forum
@endsection
@section('content')
	<main id="tt-pageContent" class="tt-offset-small">
    <div class="container">
        @if ($topics->isNotEmpty())
        <div class="tt-topic-list">
            <div class="tt-list-header">
                <div class="tt-col-topic">Topic</div>
                <div class="tt-col-category">Category</div>
                <div class="tt-col-category">Created</div>
            </div>
                @foreach ($topics as $item)
            <div class="tt-item">
                <div class="tt-col-avatar">
                    <svg class="tt-icon">
                      <use xlink:href="#icon-ava-c"></use>
                    </svg>
                </div>
                <div class="tt-col-description">
                    <h6 class="tt-title"><a href="{{route('topics.show', ['id' => $item->id] )}}">
                        {{\Illuminate\Support\Str::limit($item->judul, 50, $end='...')}}
                    </a></h6>
                    <div class="row align-items-center no-gutters">
                        <div class="col-11">
                            <ul class="tt-list-badge">
                                <li class="show-mobile"><a href="#"><span class="tt-color04 tt-badge">{{$item->kategori->nama}}</span></a></li>
                                <li><a >Created By:</a></li>
                                <li><a href="{{route('profile.show', ['profile' => $item->user_id])}}"><span class="tt-badge">{{$item->user->name}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tt-col-category"><span class="tt-color04 tt-badge">{{$item->kategori->nama}}</span></div>
                <div class="tt-col-category hide-mobile">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</div>
            </div>
            @endforeach
            @else
                <div class="tt-wrapper-inner">
                    <h4 class="tt-title-separator"><span>No Topic Found :(</span></h4>
                </div>
                @endif
            <div class="tt-row-btn">
                <button type="button" class="btn-icon js-topiclist-showmore">
                    <svg class="tt-icon">
                      <use xlink:href="#icon-load_lore_icon"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</main>
@endsection