@extends('layouts.master')
@section('title')
	Edit Replies - Forum
@endsection
@section('content')
<head>
	<script src="https://cdn.tiny.cloud/1/x9bh8x11ihsv65kouujvtlxkhasywr91avyh60k7ubm1bfh4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
      var editor_config = {
    path_absolute : "/",
    selector: 'textarea#tinyMCE',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
    </script>
</head>
@if (Auth::user()->id === $reply->user_id)
    <main id="tt-pageContent">
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Edit Topic
            </h1>
            <form class="form-default form-create-topic" action="{{route('replies.update', ['topic' => $reply->pertanyaan_id, 'reply' => $reply->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="inputTopicTitle">Replied To</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="judul" class="form-control" id="judul" value="{{$reply->topic->judul}}" readonly>
                    </div>
                </div>
                <div class="pt-editor">
                    <h6 class="pt-title">Reply Message</h6>
                    <div class="form-group">
                        <input type="hidden" name="replyId" value="{{$reply->id}}">
                        <textarea name="message" id="tinyMCE" class="form-control tinyMCE" rows="5" placeholder="Lets get started">{{$reply->konten}}</textarea>
                    </div>
                     <div class="row">
                        <div class="col-auto ml-md-auto"> 
                            <button type="submit" class="btn btn-secondary btn-width-lg">Edit</button>
                        </div>
                    </div>
                </div>
                </form>
        </div>
    </div>
	@else
		@include('error.error404')
	@endif
</main>
@endsection