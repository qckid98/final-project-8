@extends('layouts.master')

@section('content')
<main id="tt-pageContent" class="tt-offset-none">
    <div class="container">
        <div class="tt-loginpages-wrapper">
            <div class="tt-loginpages">
                <a href="index.html" class="tt-block-title">
                    <img src="images/logo.png" alt="">
                    <div class="tt-title">
                        Welcome to Forum
                    </div>
                    <div class="tt-description">
                        Join the forum to unlock true power of community.
                    </div>
                </a>
                <form class="form-default" action="{{ route('register') }}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="loginUserName">Name</label>
                        <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" required autocomplete="name" autofocus>
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="loginUserEmail">Email</label>
                        <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" id="email" required autocomplete="email">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="loginUserPassword">Password</label>
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" required autocomplete="new-password">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="password-confirm">Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control" id="password-confirm"  required autocomplete="new-password">
                    </div>
                    <div class="row">
                        <div class="col-6 form-group">
                            <label>Gender</label>
                            <br>
                            <select name="gender" id="gender" class="form-control">
                                <option value="Pria">Pria</option>
                                <option value="Wanita">Wanita</option>
                            </select>
                            @error('gender')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-6 form-group">
                            <label>Age</label>
                            <input type="number" name="umur" class="form-control @error('umur') is-invalid @enderror" id="umur" required autocomplete="umur">
                            @error('umur')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Address</label>
                        <textarea name="alamat" id="alamat" rows="2" class="form-control" required autocomplete="alamat"></textarea>
                        @error('alamat')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Biodata</label>
                        <textarea name="bio" id="bio" rows="2" class="form-control" required autocomplete="bio"></textarea>
                        @error('bio')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-secondary btn-block">Create my account</button>
                    </div>
                    <p>Already have an account? <a href="/login" class="tt-underline">Login here</a></p>
                    <div class="tt-notes">
                        By signing up, signing in or continuing, I agree to
                        Forum19’s <a href="#" class="tt-underline">Terms of Use</a> and <a href="#" class="tt-underline">Privacy Policy.</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
