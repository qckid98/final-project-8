@extends('layouts.master');
@section('title')
    Edit Category - Forum
@endsection
@section('content')
<main id="tt-pageContent">
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Edit Category
            </h1>
            <form class="form-default form-create-topic" action="/kategori/{{$kategori->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Category Title</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Subject of your Category" value="{{$kategori->nama}}">
                    </div>
                    @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="pt-editor">
                    <h6 class="pt-title">Description</h6>
                    <div class="form-group">
                        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="5" placeholder="Describe the Category">{{$kategori->deskripsi}}</textarea>
                        @error('deskripsi')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                     <div class="row">
                        <div class="col-auto ml-md-auto">
                            <a href="/kategori" class="btn btn-secondary btn-width-lg">Back</a>
                            <button type="submit" class="btn btn-secondary btn-width-lg">Update Category</button>
                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection