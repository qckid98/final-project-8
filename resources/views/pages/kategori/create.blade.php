@extends('layouts.master');
{{-- @push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css"/>
@endpush --}}
@section('title')
    Create Category - Forum
@endsection
@section('content')
<main id="tt-pageContent">
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Create New Category
            </h1>
            <form class="form-default form-create-topic" action="/kategori" method="POST">
                @csrf
                <div class="form-group">
                    <label for="nama">Category Title</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Subject of your Category">
                    </div>
                    @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                    @enderror
                </div>
                <div class="pt-editor">
                    <h6 class="pt-title">Description</h6>
                    <div class="form-group">
                        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="5" placeholder="Describe the Category"></textarea>
                        @error('deskripsi')
                        <div class="alert alert-danger">
                            {{$message}}
                        </div>
                        @enderror
                    </div>
                     <div class="row">
                        <div class="col-auto ml-md-auto">
                            <a href="/kategori" class="btn btn-secondary btn-width-lg">Back</a>
                            <button type="submit" class="btn btn-secondary btn-width-lg">Create Category</button>
                            
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
@endsection