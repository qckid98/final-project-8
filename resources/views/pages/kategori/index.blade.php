@extends('layouts.master');

@section('title')
    Category - Forum
@endsection
@section('content')
<main id="tt-pageContent" class="tt-offset-small">
    <div class="container">
        <div class="tt-topic-list">
            <div class="tt-list-header">
                <div class="tt-col-topic">Description</div>
                <div class="tt-col-category">Category</div>
                <div class="tt-col-value">Edit</div>
                <div class="tt-col-value">Delete</div>
            </div>
            @forelse ($kategori as $key=>$values)
            <div class="tt-item">
                <div class="tt-col-avatar">
                    <svg class="tt-icon">
                      <use xlink:href="#icon-ava-c"></use>
                    </svg>
                </div>
                <div class="tt-col-description">
                    <h6 class="tt-title"><a>
                        {{$values->deskripsi}}
                    </a></h6>
                    <div class="row align-items-center no-gutters">
                        <div class="col-11">
                            <ul class="tt-list-badge">
                                <li class="show-mobile"><a href="#"><span class="tt-color04 tt-badge">{{$values->nama}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tt-col-category"><span class="tt-color04 tt-badge">{{$values->nama}}</span></div>
                <div class="tt-col-value">
                <a href="/kategori/{{$values->id}}/edit" class="tt-color03 tt-badge ">Edit</a>
                </div>
                <div class="tt-col-value">
                    <form action="/kategori/{{$values->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button class="tt-color08 tt-badge"  type="submit">Delete</button>
                    </form>
                    </div>
            </div>
            @empty
            <tr>
                <td colspan="4" align="center">No data</td>
            </tr>
            @endforelse

{{-- <div class="container">
    <a href="/kategori/create" class="btn btn-secondary float-right">Tambah Kategori</a>
    <table class="table" id="myTable">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $key=>$values)
            <tr class="text-justify">
                <th scope="row">{{$key+1}}</th>
                <td style="width:15%;">{{$values->nama}}</td>
                <td style="width:65%;">{{$values->deskripsi}}</td>
                <td class="text-center">
                    <a href="/kategori/{{$values->id}}/edit" class="btn btn-info">Edit</a>
                    <form action="/kategori/{{$values->id}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger"  type="submit">Delete</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No data</td>
            </tr>  
            @endforelse
        </tbody>
    </table>
</div> --}}
</main>
@endsection