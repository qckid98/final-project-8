@extends('layouts.master')
@section('title')
    {{\Illuminate\Support\Str::limit($profile->user->name, 22, $end='..')}} - Profile
@endsection
@section('content')
    <main id="tt-pageContent" class="tt-offset-small">
    <div class="tt-wrapper-section">
        <div class="container">
            <div class="tt-user-header">
                <div class="tt-col-avatar">
                    <div class="tt-icon">
                       <img class="tt-icon" src="{{asset('storage/avatars/' . $profile->user->avatar)}}">
                    </div>
                </div>
                <div class="tt-col-title">
                    <div class="tt-title">
                        <a href="#">{{$profile->user->name}}</a>
                    </div>
                    <ul class="tt-list-badge">
                        <li><a href="#"><span class="tt-color16 tt-badge">Thread : {{$userCount->topics_count}}</span></a></li>
                        <li><a href="#"><span class="tt-color15 tt-badge">Replies : {{$userCount->replies_count}}</span></a></li>
                    </ul>
                </div>
                </div>
            </div>
        </div>
    <div class="container">
        <div class="tt-tab-wrapper">
            <div class="tt-wrapper-inner">
                <ul class="nav nav-tabs pt-tabs-default" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#tt-tab-02" role="tab"><span>Threads</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tt-tab-03" role="tab"><span>Replies</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tt-tab-10" role="tab"><span>Profile</span></a>
                    </li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane tt-indent-none show active" id="tt-tab-02" role="tabpanel">
                    <div class="tt-topic-list">
                        <div class="tt-list-header">
                            <div class="tt-col-topic">Topic</div>
                            <div class="tt-col-category">Category</div>
                            <div class="tt-col-category">About</div>
                        </div>
                        @foreach ($topic as $item)
                        <div class="tt-item">
                            <div class="tt-col-description">
                               <h6 class="tt-title"><a href="{{route('topics.show', ['id' => $item->id])}}">
                                    {{\Illuminate\Support\Str::limit($item->judul, 42, $end='...')}}
                                </a></h6>
                                <div class="row align-items-center no-gutters">
                                    <div class="col-11">
                                        <ul class="tt-list-badge">
                                            <li class="show-mobile"><a href="#"><span class="tt-color03 tt-badge">{{$item->kategori->nama}}</span></a></li>
                                            <li class="hide-mobile"><a>Created At:</a></li>
                                            <li class="hide-mobile"><span>{{$item->created_at->format('D, d-M-Y')}}</span></li>
                                            <li><a class="show-mobile">Created At:</a></li>
                                            <li class="show-mobile"><a href="#"><span class="tt-color04 tt-badge">{{$item->created_at->format('D, d-M-Y')}}</span></a></li>
                                            <li class="show-mobile"><a href="#"><span class="tt-color07 tt-badge">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="tt-col-category"><span class="tt-color03 tt-badge">{{$item->kategori->nama}}</span></div>
                            <div class="tt-col-category hide-mobile">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</div>
                        </div>
                        @endforeach
                        <div class="tt-row-btn">
                            <button type="button" class="btn-icon js-topiclist-showmore">
                                <svg class="tt-icon">
                                  <use xlink:href="#icon-load_lore_icon"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tt-indent-none" id="tt-tab-03" role="tabpanel">
                     <div class="tt-topic-list">
                        <div class="tt-list-header">
                            <div class="tt-col-topic">Topic</div>
                            <div class="tt-col-category">Category</div>
                            <div class="tt-col-value">Activity</div>
                        </div>
                        @forelse ($reply as $item)
                        <div class="tt-item">
                            <div class="tt-col-avatar">
                              <img class="tt-icon" src="{{asset('storage/avatars/' . $item->user->avatar)}}">
                            </div>
                            <div class="tt-col-description">
                                <h6 class="tt-title"><a href="{{route('topics.show', ['id' => $item->pertanyaan_id])}}">
                                    {{\Illuminate\Support\Str::limit($item->topic->judul, 42, $end='...')}}
                                </a></h6>
                                <div class="row align-items-center no-gutters hide-desktope">
                                    <div class="col-9">
                                        <ul class="tt-list-badge">
                                            <li class="show-mobile"><a href="#"><span class="tt-color06 tt-badge">{{$item->topic->kategori->nama}}</span></a></li>
                                        </ul>
                                    </div>
                                    <div class="col-3 ml-auto show-mobile">
                                        <div class="tt-value">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</div>
                                    </div>
                                </div>
                                <div class="tt-content">
                                    Reply: {{strip_tags($item->konten)}}
                                </div>
                            </div>
                            <div class="tt-col-category"><a href="#"><span class="tt-color06 tt-badge">{{$item->topic->kategori->nama}}</span></a></div>
                            <div class="tt-col-value-large hide-mobile">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</div>
                        </div>
                        @empty
                        <div class="tt-wrapper-inner">
                                <h4 class="tt-title-separator"><span>No Topic Found :(</span></h4>
                        </div>
                        @endforelse
                        <div class="tt-row-btn">
                            <button type="button" class="btn-icon js-topiclist-showmore">
                                <svg class="tt-icon">
                                  <use xlink:href="#icon-load_lore_icon"></use>
                                </svg>
                            </button>
                        </div>
                    </div>
                </div>
                @if (Auth::user()->id == $profile->user_id)
                <div class="tab-pane" id="tt-tab-10" role="tabpanel">
                    <div class="tt-wrapper-inner">
                        <form class="form-default form-create-topic" action="{{route('profile.update', ['profile' => $profile->user_id])}}" method="post">
                            @csrf
                    @method('PUT')
                    <div class="form-group">
                        <input type="hidden" name="asda" class="form-control" id="asda">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="name" class="form-control" id="name" value="{{$profile->user->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="email" class="form-control" id="email" value="{{$profile->user->email}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="umur">Age</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="umur" class="form-control" id="umur" value="{{$profile->umur}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat">Address</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="alamat" class="form-control" id="alamat" value="{{$profile->alamat}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="biodata">Biodata</label>
                    <div class="form-group">
                        <textarea name="biodata" id="biodata" class="form-control" rows="5">{{$profile->biodata}}</textarea>
                    </div>
                     <div class="row">
                        <div class="col-auto ml-md-auto">
                            <button type="submit" class="btn btn-secondary btn-width-lg">Edit</button>
                        </div>
                    </div>
                    </form>
                </div>
                    </div>
                </div>
                @else
                <div class="tab-pane" id="tt-tab-10" role="tabpanel">
                    <div class="tt-wrapper-inner">
                        <form class="form-default form-create-topic">
                <div class="form-group">
                    
                        <input type="hidden" name="asda" class="form-control" id="asda">
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="name" class="form-control" id="name" value="{{$profile->user->name}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="email" class="form-control" id="email" value="{{$profile->user->email}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="umur">Age</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="umur" class="form-control" id="umur" value="{{$profile->umur}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat">Address</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="alamat" class="form-control" id="alamat" value="{{$profile->alamat}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="biodata">Biodata</label>
                    <div class="form-group">
                        <textarea name="biodata" id="biodata" class="form-control" rows="5">{{$profile->biodata}}</textarea>
                    </div>
                </form>
                </div>
                    </div>
                    @endif
                </div>
            </div>
</main>
@endsection