@extends('layouts.master')
@section('title')
    {{\Illuminate\Support\Str::limit($topic->judul, 20, $end='...')}} - Topic - Forum
@endsection
@section('content')
<head>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="https://cdn.tiny.cloud/1/x9bh8x11ihsv65kouujvtlxkhasywr91avyh60k7ubm1bfh4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
		<script>
      var editor_config = {
    path_absolute : "/",
    selector: 'textarea#tinyMCE',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
    </script>
    <script>
        <script src="{{asset('js/animatescroll.js')}}">
    </script>
</head>
    <main id="tt-pageContent">
    <div class="container">
        <div class="tt-single-topic-list">
            <div class="tt-wrapper-inner">
            <h4 class="tt-title-separator"><span>Thread</span></h4>
        </div>
            <div class="tt-item">
                 <div class="tt-single-topic">
                    <div class="tt-item-header">
                        <div class="tt-item-info info-top">
                            <div class="tt-avatar-icon">
                                <img class="tt-icon" src="{{asset('storage/avatars/' . $topic->user->avatar)}}" style="max-width: 40px; max-heigh: 40px">
                            </div>
                            <div class="tt-avatar-title">
                               <a href="{{route('profile.show', ['profile' => $topic->user_id])}}">{{$topic->user->name}}</a>
                            </div>
                            <a href="#" class="tt-info-time">
                                <i class="tt-icon"><svg><use xlink:href="#icon-time"></use></svg></i>{{$topic->created_at->format('D, d-M-Y')}}
                            </a>
                        </div>
                        <h3 class="tt-item-title">
                            <span>{{$topic->judul}}</span>
                        </h3>
                        <div class="tt-item-tag">
                            <ul class="tt-list-badge">
                                <li><a href="#"><span class="tt-color03 tt-badge">{{$topic->kategori->nama}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="tt-item-description">
                        {!!$topic->pertanyaan!!}
                    </div>
                    <div class="tt-item-info info-bottom">
                        @if(Auth::check() && Auth::user()->id == $topic->user_id)
                        <a href="{{route('topics.edit', ['id' => $topic->id])}}"><span class="tt-color05 tt-badge">Edit</span></a>
                        <div class="col-separator"></div>
                        <form action="{{route('topics.destroy', ['id' => $topic->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                        <button type="submit" class="tt-color05 tt-badge">Delete</button>
                        </form>
                        @endif
                        </a>
                    </div>
                </div>
            </div>
            <div class="tt-wrapper-inner">
            <h4 class="tt-title-separator"><span>Replies</span></h4>
        </div>
        @forelse($replies as $reply)
            <div class="tt-item">
                 <div class="tt-single-topic">
                    <div class="tt-item-header pt-noborder">
                        <div class="tt-item-info info-top">
                            <div class="tt-avatar-icon">
                                <img class="tt-icon" src="{{asset('storage/avatars/' . $reply->user->avatar)}}" style="max-width: 40px; max-heigh: 40px">
                            </div>
                            <div class="tt-avatar-title">
                               <a href="#">{{$reply->user->name}}</a>
                            </div>
                            <a href="#" class="tt-info-time">
                                <i class="tt-icon"><svg><use xlink:href="#icon-time"></use></svg></i>{{$reply->created_at->format('D, d-M-Y')}}
                            </a>
                        </div>
                    </div>
                    <div class="tt-item-description">
                        {!!$reply->konten!!}
                    </div>
                    <div class="tt-item-info info-bottom">
                        @if(Auth::check() && Auth::user()->id == $reply->user_id)
                        <form action="{{route('replies.edit', ['topic' => $reply->pertanyaan_id, 'reply' => $reply->id])}}" method="get">
                        @csrf
                        <input type="hidden" name="replyId" value="{{$reply->id}}">
                        <button type="submit" class="tt-color05 tt-badge">Edit</button>
                        </form>
                        <div class="col-separator"></div>
                        <form action="{{route('replies.destroy', ['topic'=>$reply->pertanyaan_id,'reply' => $reply->id])}}" method="POST">
                            @csrf
                            @method('DELETE')
                        <input type="hidden" name="replyId" value="{{$reply->id}}">
                        <button type="submit" class="tt-color05 tt-badge">Delete</button>
                        </form>
                        @endif
                        </a>
                    </div>
                </div>
            </div>
        @empty
        <div class="tt-wrapper-inner">
            <h4 class="tt-title-separator"><span>No Replies Yet.</span></h4>
        </div>
        @endforelse
        </div>
        <div class="tt-wrapper-inner">
            <h4 class="tt-title-separator"><span>You’ve reached the end of replies</span></h4>
        </div>
        @if (Auth::check() == False)
        <div class="tt-topic-list">
            <div class="tt-item tt-item-popup">
                <div class="tt-col-avatar">
                    <svg class="tt-icon">
                      <use xlink:href="#icon-ava-f"></use>
                    </svg>
                </div>
                <div class="tt-col-message">
                    Looks like you are new here. Register for free, learn and contribute.
                </div>
                <div class="tt-col-btn">
                    <a href="/login" class="btn btn-primary" >Log in</a>
                    <a href="/register" class="btn btn-secondary" >Sign up</a>
                </div>
            </div>
        </div>
        @else
        <form action="{{route('replies.store', ['topic' => $topic->id])}}" method="post">
        @csrf
        <div class="tt-wrapper-inner">
            <div class="pt-editor form-default" id="editor">
                <h6 class="pt-title">Post Your Reply</h6>
                <div class="form-group">
                    <input type="hidden" name="topic_id" value="{{$topic->id}}">
                    <textarea name="message" class="form-control tinyMCE" id="tinyMCE" rows="5"></textarea>
                </div>
                <div class="pt-row">
                    <div class="col-auto">
                    </div>
                    <div class="col-auto">
                        <button type="submit" class="btn btn-secondary btn-width-lg">Reply</button>
                    </div>
                </div>
            </div>
        </div>
        </form>
        @endif
        <div class="tt-topic-list tt-ofset-30">
            <div class="tt-list-header tt-border-bottom">
                <div class="tt-col-topic">Topic</div>
                <div class="tt-col-category">Category</div>
                <div class="tt-col-category">Created</div>
            </div>
            @forelse ($topics as $item)
            <div class="tt-item">
                <div class="tt-col-avatar">
                    <img class="tt-icon" src="{{asset('storage/avatars/' . $item->user->avatar)}}">
                </div>
                <div class="tt-col-description">
                    <h6 class="tt-title"><a href="{{route('topics.show', ['id' => $item->id] )}}">
                        {{\Illuminate\Support\Str::limit($item->judul, 50, $end='...')}}
                    </a></h6>
                    <div class="row align-items-center no-gutters">
                        <div class="col-11">
                            <ul class="tt-list-badge">
                                <li class="show-mobile"><a href="#"><span class="tt-color04 tt-badge">{{$item->kategori->nama}}</span></a></li>
                                <li><a >Created By:</a></li>
                                <li><a href="{{route('profile.show', ['profile' => $item->user_id])}}"><span class="tt-badge">{{$item->user->name}}</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tt-col-category"><span class="tt-color04 tt-badge">{{$item->kategori->nama}}</span></div>
                <div class="tt-col-category hide-mobile">{{Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</div>
            </div>
            @empty
            <div class="tt-wrapper-inner">
                    <h4 class="tt-title-separator"><span>No Topic Found :(</span></h4>
                </div>
            @endforelse
            <div class="tt-row-btn">
                <button type="button" class="btn-icon js-topiclist-showmore">
                    <svg class="tt-icon">
                      <use xlink:href="#icon-load_lore_icon"></use>
                    </svg>
                </button>
            </div>
        </div>
    </div>
</main>
@endsection