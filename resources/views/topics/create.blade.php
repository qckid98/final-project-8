@extends('layouts.master')
@section('title')
Create Topic - Forum
@endsection
@section('content')
	<head>
	<script src="https://cdn.tiny.cloud/1/x9bh8x11ihsv65kouujvtlxkhasywr91avyh60k7ubm1bfh4/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
	<script>
      var editor_config = {
    path_absolute : "/",
    selector: 'textarea#tinyMCE',
    relative_urls: false,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table directionality",
      "emoticons template paste textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    file_picker_callback : function(callback, value, meta) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?editor=' + meta.fieldname;
      if (meta.filetype == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.openUrl({
        url : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no",
        onMessage: (api, message) => {
          callback(message.content);
        }
      });
    }
  };

  tinymce.init(editor_config);
    </script>
</head>
    <main id="tt-pageContent">
    <div class="container">
        <div class="tt-wrapper-inner">
            <h1 class="tt-title-border">
                Create New Topic
            </h1>
            <form class="form-default form-create-topic" action="{{route('topics.store')}}" method="POST">
				      @csrf
                <div class="form-group">
                    <label for="inputTopicTitle">Topic Title</label>
                    <div class="tt-value-wrapper">
                        <input type="text" name="judul" class="form-control" id="judul" placeholder="Subject of your topic">
                        <span class="tt-value-input" id="tt-value-input">99</span>
                    </div>
                    <div class="tt-note">Describe your topic well, while keeping the subject as short as possible.</div>
                </div>
                <div class="pt-editor">
                    <h6 class="pt-title">Topic Description</h6>
                    <div class="form-group">
                        <textarea name="tinyMCE" id="tinyMCE" class="form-control tinyMCE" rows="5" placeholder="Lets get started"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputTopicTitle">Category</label>
                                <select class="form-control" name="kategori_id" id="kategori_id">
									@foreach ($category as $item)
										<option value="{{$item->id}}">{{$item->nama}}</option>
									@endforeach
                                </select>
                            </div>
                        </div>
                        
                    </div>
                     <div class="row">
                        <div class="col-auto ml-md-auto">
                            <button type="submit" class="btn btn-secondary btn-width-lg">Create Post</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
<script>
let textArea = document.getElementById("judul");
let characterCounter = document.getElementById("tt-value-input");
const maxNumOfChars = 99;
const countCharacters = () => {
let numOfEnteredChars = textArea.value.length;
let counter = maxNumOfChars - numOfEnteredChars;
characterCounter.textContent = counter + "/99";
};
textArea.addEventListener("input", countCharacters);
</script>
@endsection